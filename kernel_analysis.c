/* Isolated Timing Analysis */

#include <stdlib.h>
#include <stdio.h>
#include "mf_4.h"
#include "fft_8.h"

int main(int argc, char *argv[]) {
	if(argc < 2) {
		fprintf(stderr,"Enter the of RBs\n");
		exit(EXIT_FAILURE);
	}
	int nmbRB = atoi(argv[1]);
	
	/* Phase 1 computations */
	mf_test(nmbRB);
	fft_test(nmbRB); // Used twice in phase 1, also used in phase 3
	window_test(nmbRB);
	
	/* Phase 2 computations */
	mmse_by_cholsolve_4xX_complex_partiled_test(nmbRB);
	
	/* Phase 3 computations */
	ant_comb_test(nmbRB);
	
	/* Phase 4 Computation */
	soft_demap_pthread_test(nmbRB);
	
	/* A typical lte phy ul application */
	//phy(argc,argv);					/* Barrier Synchronization */
	//phy_ms(argc,argv);				/* Master/Slave Synchronization */
	//phy_ms_mue(argc,argv);				/* Master/Slave Synchronization - Multi-UE */
	phy_ms_mue_serial(argc,argv);		/* Master/Slave Synchronization - Multi-UE (serial) */
	//compute_demap_test(argc,argv);
}
