#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include "kernel_def.h"
#include "common_utils.h"
#include "util.h"

#ifndef MAX_SUBFRAMES1
#define MAX_SUBFRAMES1 200
#define DELTA 1000
#define N 20050
#endif

typedef struct __sf_t {
	ue_t	ue;
	struct timespec gen_time_stamp;
	struct __sf_t *next;
} sf_t;

typedef struct __sf_queue_t {
	pthread_mutex_t lock;
	sf_t *first;
	sf_t *last;
	volatile int qcount;
} sf_queue_t;

typedef struct __thr_args_t {
	sf_queue_t *sf_queue;
	int	 thr_id;
} thr_args_t;

/* Global Variables */
sf_queue_t   sf_queue;
input_data_t global_input_data; /* Global Input Data */
pthread_t 	exec_engine1;
thr_args_t	exec_args1;
pthread_t 	exec_engine2;
thr_args_t	exec_args2;

/* generate a ue with a given subframe id */
void gen_sf(int subframe_id,  \
			 int nmb_rb,	  \
			 int nmb_layer,	  \
			 mod_t mod,		  \
			 sf_t *sf) {
	gen_ue(subframe_id,nmb_rb,nmb_layer,mod,&(sf->ue));
	clock_gettime(CLOCK_REALTIME,&(sf->gen_time_stamp));
}

/* Code to generate sf1 */
void sf_generator(void *arg) {
	int subframe_count = 0;
	sf_t	*sf;

	gen_input_data(&pregen_data);
	struct timespec t_curr, t_prev;
	alarm_init(DELTA);
	clock_gettime(CLOCK_REALTIME,&t_prev);
	while(subframe_count++ < MAX_SUBFRAMES1-1) {
		
		/* Generate the Next sf */
		sf		= (sf_t*)malloc(sizeof(sf_t));
		gen_sf(subframe_count,100,4,MOD_64QAM,sf);
		printf("Subframe Generated %d\n",subframe_count);

		/* Alarm after every DELTA time */
		wait_for_alarm();

		/* Attach the time stamp to the sf */
		clock_gettime(CLOCK_REALTIME,&(sf->gen_time_stamp));
		
		/* Insert sf into the subframe queue */
		pthread_mutex_lock(&sf_queue.lock);
		if(sf_queue.qcount <= 2) {
			if(sf_queue.first == NULL) {
				sf_queue.first		= sf;
				sf_queue.last		= sf;
				sf_queue.qcount++;
			}
			else {
				sf_queue.last->next	= sf;
				sf_queue.last		= sf;
				sf_queue.qcount++;
			}
		} else {
			printf("subframe@%d dropped\n",subframe_count);
			free(sf);
		}
		pthread_mutex_unlock(&sf_queue.lock);
		
		clock_gettime(CLOCK_REALTIME,&t_curr);
		printf("Through put = %lld\n",calculate_time_diff_spec(t_curr,t_prev));
	}
}

/* "Execute" a subframe */
void *sf_execute_dummy(void *arg){
	thr_args_t *thr_arg = (thr_args_t*)arg;
	int thr_id			= thr_arg->thr_id;
	affinity_set_cpu(thr_id+20);
 
	struct timespec t1, t2;
	int i;
	int lock_status;

	sf_t *sf = NULL;

	while(1) {

		sf = NULL;

		/* Deq an sf from sf_queue/set */
		lock_status = pthread_mutex_trylock(&(thr_arg->sf_queue->lock));
		if(lock_status == 0) {	/* Lock is acquired */
			if (thr_arg->sf_queue->first != NULL) {
				if(thr_arg->sf_queue->first == NULL) {
					fprintf(stderr,"thr_id@%d : Invalid NULL value of first pointer, qcount = %d\n",thr_id,thr_arg->sf_queue->qcount);
					exit(EXIT_FAILURE);
				}
				sf 							= thr_arg->sf_queue->first;
				thr_arg->sf_queue->first	= thr_arg->sf_queue->first->next;
				//thr_arg->sf_queue->qcount--;
			}
			pthread_mutex_unlock(&(thr_arg->sf_queue->lock));
		}

		if(sf) {
			/* Dummy Execution Modelled as sleep */
			clock_gettime(CLOCK_REALTIME,&t1);
			usleep(N);
			clock_gettime(CLOCK_REALTIME,&t2);

			/* Optionally print the execution times */
			printf("thr_id@%d : Sf@%d, ExecTime@%lld, TotalTime@%lld\n",\
			thr_id,sf->ue.subframe,\
			calculate_time_diff_spec(t2,t1),\
			calculate_time_diff_spec(t2,sf->gen_time_stamp));
			
			pthread_mutex_lock(&(thr_arg->sf_queue->lock));
			thr_arg->sf_queue->qcount--;
			//free(sf);
			pthread_mutex_unlock(&(thr_arg->sf_queue->lock));
		}
	}
}

/* Test code */
void sf_generator_test() {
	affinity_set_cpu(0);

	/* Thread Creation */
	int rc = 0;
	exec_args1.thr_id 	= 1;
	exec_args1.sf_queue	= &sf_queue;
	exec_args2.thr_id 	= 2;
	exec_args2.sf_queue	= &sf_queue;
	rc = pthread_create(&exec_engine1,NULL,sf_execute_dummy,&exec_args1);
	rc = pthread_create(&exec_engine2,NULL,sf_execute_dummy,&exec_args2);

	sf_generator(NULL);

	/* Wait for the subframe queue to be empty */
	bool flag = false;
	while(sf_queue.qcount > 0) {
		//if(!flag) {
		//	printf("qcount value = %d\n",sf_queue.qcount);
		//	flag = true;
		//}
	}
}
