#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <complex.h>
#include <assert.h>
#include "kernel_def.h"
#include "common_utils.h"
#include "util.h"
#include "mkl_types.h"

void affinity_set_cpu(int _id) {
	int cpuid = _id % NUMCORES;
	cpu_set_t set;
	CPU_ZERO(&set);
	CPU_SET(cpuid, 		&set);
	CPU_SET(cpuid+64, 	&set);
	CPU_SET(cpuid+128, 	&set);
	CPU_SET(cpuid+192, 	&set);
	int rc = pthread_setaffinity_np(pthread_self(),sizeof(cpu_set_t),&set);
	//printf("task_aff:%d affined to cpuid:%d\n",_id,cpuid);
	if(rc != 0) {
		fprintf(stderr,"Set Affinity failed for tid#%u\n",_id);
		exit(EXIT_FAILURE);
	}
}

/* Randomly generate input data */
void gen_input_data(input_data_t *in) {
	int slot, symb, sc, layer, rx;

	for(slot = 0; slot < NMB_SLOT; slot++) {
		for(sc = 0; sc < MAX_SC; sc++) {
			for(symb = 0; symb < OFDM_IN_SLOT; symb++) {
					for(rx = 0; rx < RX_ANT; rx++)
						gen_scalar(&in->in_data[slot][symb][rx][sc]);
			}

			for(layer = 0; layer < MAX_LAYERS; layer++)
				gen_scalar(&in->in_rs[slot][sc][layer]);

			gen_scalar(&in->fft_tf[slot][sc]);
		}
	}
}

/* generate a ue with a given subframe id */
void gen_ue(int subframe_id,  \
			 int nmb_rb,	  \
			 int nmb_layer,	  \
			 mod_t mod,		  \
			 ue_t *ue) {

	if(!ue) {
		fprintf(stderr,"Memory allocation for ue failed...\n");
		exit(EXIT_FAILURE);
	}

	/* Assign data */
	//gen_input_data(&ue->data);
	ue->data		= pregen_data;	/* Assign a pregenerated data to minimize time */
	
	/* Assign meta information */
	ue->subframe	= subframe_id;
	ue->start_rb	= 0;
	ue->nmb_rb		= nmb_rb;
	ue->nmb_layer	= nmb_layer;
	ue->mod			= MOD_64QAM;
	ue->ueid		= 0;
	ue->last		= true;
}

/* generate a ue with a given subframe id */
void gen_sf(int subframe_id,  \
			 int nmb_rb,	  \
			 int nmb_layer,	  \
			 mod_t mod,		  \
			 ue_t *mue,		  \
			 int size) {

	if(!mue) {
		fprintf(stderr,"Memory allocation for ue failed...\n");
		exit(EXIT_FAILURE);
	}
	assert(size <= MAX_USERS);

	int i, rb = 0, ueid = 0;
	for(i = 0; i < size; i++) {
		/* Assign data */
		mue[i].data			= pregen_data;	/* Assign a pregenerated data to minimize time */
		
		/* Assign meta information */
		mue[i].subframe		= subframe_id;
		mue[i].start_rb		= rb;
		mue[i].nmb_rb		= nmb_rb;
		mue[i].nmb_layer	= nmb_layer;
		mue[i].mod			= MOD_64QAM;
		mue[i].ueid			= ueid++;
		mue[i].last			= (i == size-1)?true:false;

		mue[i].valid		= true;
		/* nmb_rb: Number of resource blocks per user */
		rb += nmb_rb;
	}
	for(i = size; i < MAX_USERS; i++)
		mue[i].valid		= false;
}


void dump_micf(char *file) {
	FILE* fd = fopen(file,"w");

	if(fd <= 0) {
		fprintf(stderr,"Failed to open micf dump file \n");
		exit(EXIT_FAILURE);
	}

	int nth = 16;
	int subframe, user, slot, tid;
	//for(tid = 0; tid < nth; tid++)
	//	fprintf(fd,"taskid_start:%d,comp_time:%d,micf_end:%d",tid,tid,tid);
	//fprintf(fd,"task_start,comp_time,micf_end");
	
	//fprintf(fd,"micf_end");
	//fprintf(fd,"\n");

	for(subframe = 0; subframe < MAX_SUBFRAMES; subframe++) {
		for(user = 0; user < MAX_USERS; user++) {
		for(slot = 0; slot < NMB_SLOT; slot++) {
			//fprintf(fd,"task_start,comp_time,micf_end\n");
			fprintf(fd,"t1,t2,t3\n");
			for(tid = 0; tid < nth; tid++) {
				fprintf(fd,"%lld,%lld,%lld\n",\
						calculate_time_diff_spec(micf_task_timer_start[subframe][user][slot][tid], micf_start[subframe][user][slot]),\
						calculate_time_diff_spec(micf_task_timer_stop[subframe][user][slot][tid], micf_task_timer_start[subframe][user][slot][tid]),\
						calculate_time_diff_spec(micf_stop[subframe][user][slot], micf_task_timer_stop[subframe][user][slot][tid]));
						
			}
			//fprintf(fd,"%lld",\
			//calculate_time_diff_spec(micf_stop[subframe][user][slot],micf_start[subframe][user][slot]));
			fprintf(fd,"\n\n");
		}
		}
	}
	fclose(fd);
}

void dump_combwc(char *file) {
	FILE* fd = fopen(file,"w");

	if(fd <= 0) {
		fprintf(stderr,"Failed to open combwc dump file \n");
		exit(EXIT_FAILURE);
	}
	int subframe, user, slot, tid;
	for(tid = 0; tid < MAX_THREADS; tid++)
		fprintf(fd,"taskid:%d,",tid);
	fprintf(fd,"combwc");
	fprintf(fd,"\n");

	for(subframe = 0; subframe < MAX_SUBFRAMES; subframe++) {
		for(user = 0; user < MAX_USERS; user++) {
		for(slot = 0; slot < NMB_SLOT; slot++) {
			for(tid = 0; tid < MAX_THREADS; tid++) {
				fprintf(fd,"%lld,",\
						calculate_time_diff_spec(combwc_task_timer_stop[subframe][user][slot][tid], combwc_task_timer_start[subframe][user][slot][tid]));
			}
			fprintf(fd,"%lld",\
			calculate_time_diff_spec(combwc_stop[subframe][user][slot],combwc_start[subframe][user][slot]));
			fprintf(fd,"\n");
		}
		}
	}
	fclose(fd);
}

void dump_antcomb(char *file) {
	FILE* fd = fopen(file,"w");

	if(fd <= 0) {
		fprintf(stderr,"Failed to open antcomb dump file \n");
		exit(EXIT_FAILURE);
	}
	int subframe, user, slot, tid;
	for(tid = 0; tid < MAX_THREADS; tid++)
		fprintf(fd,"taskid:%d,",tid);
	fprintf(fd,"antcomb");
	fprintf(fd,"\n");

	for(subframe = 0; subframe < MAX_SUBFRAMES; subframe++) {
		for(user = 0; user < MAX_USERS; user++) {
		for(slot = 0; slot < NMB_SLOT; slot++) {
			for(tid = 0; tid < MAX_THREADS; tid++) {
				fprintf(fd,"%lld,",\
						calculate_time_diff_spec(antcomb_task_timer_stop[subframe][user][slot][tid], antcomb_task_timer_start[subframe][user][slot][tid]));
			}
			fprintf(fd,"%lld",\
			calculate_time_diff_spec(antcomb_stop[subframe][user][slot],antcomb_start[subframe][user][slot]));
			fprintf(fd,"\n");
		}
		}
	}
	fclose(fd);
}

void dump_demap(char *file) {
	FILE* fd = fopen(file,"w");

	if(fd <= 0) {
		fprintf(stderr,"Failed to open demap dump file \n");
		exit(EXIT_FAILURE);
	}
	int subframe, user, tid;
	for(tid = 0; tid < MAX_THREADS; tid++)
		fprintf(fd,"taskid:%d,",tid);
	fprintf(fd,"demap");
	fprintf(fd,"\n");

	for(subframe = 0; subframe < MAX_SUBFRAMES; subframe++) {
		for(user = 0; user < MAX_USERS; user++) {
			for(tid = 0; tid < MAX_THREADS; tid++) {
				fprintf(fd,"%lld,",\
						calculate_time_diff_spec(demap_task_timer_stop[subframe][user][tid], demap_task_timer_start[subframe][user][tid]));
			}
			fprintf(fd,"%lld",\
			calculate_time_diff_spec(demap_stop[subframe][user],demap_start[subframe][user]));
			fprintf(fd,"\n");
		}
	}
	fclose(fd);
}

void dump_sf(char *file) {
	FILE* fd = fopen(file,"w");

	if(fd <= 0) {
		fprintf(stderr,"Failed to open sf dump file \n");
		exit(EXIT_FAILURE);
	}
	int subframe;
	fprintf(fd,"sample,exec\n");
	for(subframe = 0; subframe < MAX_SUBFRAMES; subframe++) {
		fprintf(fd,"%d, %lld\n",subframe,calculate_time_diff_spec(sf_stop[subframe],sf_start[subframe]));
	}
	fclose(fd);
}
