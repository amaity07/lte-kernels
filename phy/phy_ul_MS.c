#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include "kernel_def.h"
#include "common_utils.h"
#include "util.h"
#include "ant_comb_7.h"
#include "chest_5.h"
#include "complex_def.h"
#include "crc_13.h"
#include "fft_8.h"
#include "interleave_11.h"
#include "mf_4.h"
#include "soft_demap_9.h"
#include "turbo_dec_12.h"
#include "weight_calc_6.h"

/* Parallelized symbol demap */
/*
 * The Demap code in src/soft_demap_9.c gives
 * some timing anomalies. Even though
 * the code is exactly the same
 * The Most Likely Culprit is the IPO/Multifile
 * optimization by icc, please verify
 */
static inline void soft_demap_pthread1(MKL_Complex8 *in, float scaling_factor, int mod,\
						int n, char* out, int num_threads, int id) {
  	float L;
	int32_t 	 L_fixedpt;
	int16_t		 real_fixedpt, imag_fixedpt;
  	int i,k,ind;

    unsigned int work_per_thread = n/num_threads;

  switch (mod) {
      case MOD_64QAM:
    {
      //printf("demap-id:%d,executing 64qam\n",id);
      int32_t 	temp_real, temp_imag; 
      uint32_t 	temp_realu, temp_imagu; /* Unsigned Values */
      int32_t 	scaling_factor_fixedpt = (int32_t)scaling_factor;
      ind = 0;
      for (i = id*work_per_thread; i < (id+1)*work_per_thread; i++) {
    	ind = i*2*6;
    	for (k=0; k<6; k++) {
			/* Convert to fixed point */
      		L_fixedpt 		= k << 12; /* multiply by 4096 */
			real_fixedpt	= (int16_t)in[i].real;
			imag_fixedpt	= (int16_t)in[i].imag;
			
			/* Compute intermediate values */
      		temp_real = (((L_fixedpt - 1) - ((real_fixedpt * scaling_factor_fixedpt) >> 8) + (L_fixedpt - 1)) >> 1);
      		temp_imag = (((L_fixedpt - 1) - ((imag_fixedpt * scaling_factor_fixedpt) >> 8) + (L_fixedpt - 1)) >> 1);

			/* Restrict the values */
      		if (temp_real < 0)
      			temp_realu = 0;
      		else if (temp_real > (L_fixedpt - 1))
      			temp_realu = (L_fixedpt - 1);
      		if (temp_imag < 0)
      			temp_imagu = 0;
      		else if (temp_imag > (L_fixedpt - 1))
      			temp_imagu = (L_fixedpt - 1);

			/* Assign value to the output array */
      		out[ind++] = (temp_realu & 0x000000ff);
      		out[ind++] = (temp_imagu & 0x000000ff);
    	}
      }
    }
 
	break;

  default:
    fprintf(stderr,"(DEMAP) Modulation not supported: %i\n", mod);
  }
}

static inline void compute_micf(phy_task2_t *task) {
	int rx						= task->rx;
	int layer					= task->layer;
	int startSc					= task->startSc;
	int nmbSc					= task->nmbSc;
	int slot					= task->slotid;
	int subframe				= task->subframe;
	int taskid					= task->taskid;
	int userid					= task->userid;	

	input_data_t *ue_data_p 	= &(ue.data);
	phy_vars_t	 *phy_vars_p	= &phy_vars;
	
	clock_gettime(CLOCK_REALTIME,&micf_task_timer_start[subframe][userid][slot][taskid]);
	/* Matched Filter */
	mf(&(ue_data_p->in_data[slot][3][rx][startSc]), 	 \
	   &(ue_data_p->in_rs[slot][startSc][layer]), /* Check the indices*/ \
	   nmbSc, phy_vars_p->layer_data[layer][rx],  \
	   &(phy_vars_p->pow[layer][rx]));
	/* IFFT */
	ifft(phy_vars_p->layer_data[layer][rx],			\
		 nmbSc,										\
		 ue_data_p->fft_tf[slot],					\
		 &global_fft_handle);
	/* Windowing */
	window(phy_vars_p->layer_data[layer][rx],			\
		  phy_vars_p->pow[layer][rx],					\
		  nmbSc,										\
		  phy_vars_p->layer_data[layer][rx],			\
		  &(phy_vars_p->res_power[layer][rx]));
	/* Intermediate Data Storage */
	phy_vars_p->R[layer][rx].real = 0.0;
	phy_vars_p->R[layer][rx].imag = 0.0;
	/* FFT */
	fft(phy_vars_p->layer_data[layer][rx],nmbSc,ue_data_p->fft_tf[slot],&global_fft_handle);
	clock_gettime(CLOCK_REALTIME,&micf_task_timer_stop[subframe][userid][slot][taskid]);

	task->computed = true;
}

static inline void compute_combwc(phy_task2_t *task) {
	input_data_t *ue_data_p 	= &(ue.data);
	phy_vars_t	 *phy_vars_p	= &(phy_vars);
	int rx						= task->rx;
	int layer					= task->layer;
	int startSc					= task->startSc;
	int nmbSc					= task->nmbSc;
	int slot					= task->slotid;
	int subframe				= task->subframe;
	int taskid					= task->taskid;
	int nmbLayer				= task->nmbLayer;
	int userid					= task->userid;

	clock_gettime(CLOCK_REALTIME,&combwc_task_timer_start[subframe][userid][slot][taskid]);
		
	/* Calculate the range of subcarriers to compute on */
	assert(nmbSc % MAX_COMBWC_TASKS == 0);
	int start_tile				= (taskid)*(nmbSc/MAX_COMBWC_TASKS);
	int end_tile				= (taskid+1)*(nmbSc/MAX_COMBWC_TASKS);

	comb_w_calc_partiled(phy_vars_p->layer_data,		\
						 start_tile,end_tile,nmbLayer,	\
						 phy_vars_p->R,					\
						 phy_vars_p->comb_w);
	clock_gettime(CLOCK_REALTIME,&combwc_task_timer_stop[subframe][userid][slot][taskid]);
	task->computed = true;
}

static inline void compute_antcomb(phy_task2_t *task) {
	input_data_t *ue_data_p 	= &(ue.data);
	phy_vars_t	 *phy_vars_p	= &(phy_vars);
	int rx						= task->rx;
	int layer					= task->layer;
	int startSc					= task->startSc;
	int nmbSc					= task->nmbSc;
	int slot					= task->slotid;
	int subframe				= task->subframe;
	int taskid					= task->taskid;
	int nmbLayer				= task->nmbLayer;
	int ofdm					= task->ofdm;
	int ofdm_count				= task->ofdm_count;
	int userid					= task->userid;
	
	MKL_Complex8* in[4];
	int index_out;
	
	clock_gettime(CLOCK_REALTIME,&antcomb_task_timer_start[subframe][userid][slot][taskid]);
	in[0] = &ue_data_p->in_data[slot][ofdm][0][startSc];
	in[1] = &ue_data_p->in_data[slot][ofdm][1][startSc];
	in[2] = &ue_data_p->in_data[slot][ofdm][2][startSc];
	in[3] = &ue_data_p->in_data[slot][ofdm][3][startSc];
	/* Put all demodulated symbols in one long vector */
	index_out = nmbSc*ofdm_count + slot*(OFDM_IN_SLOT-1)*nmbSc + layer*2*(OFDM_IN_SLOT-1)*nmbSc;
	ant_comb(in, phy_vars_p->combWeight[layer], nmbSc, &phy_vars_p->symbols[index_out]);
	/* Now transform data back to time plane */
	ifft(&phy_vars_p->symbols[index_out], nmbSc, ue_data_p->fft_tf[slot], &global_fft_handle);
	
	/* Mark the task as computed */
	clock_gettime(CLOCK_REALTIME,&antcomb_task_timer_stop[subframe][userid][slot][taskid]);
	task->computed = true;
}

static inline void compute_demap(phy_task2_t *task) {
	phy_vars_t	 *phy_vars_p	= &phy_vars;//task->phy_vars_p;
	int nmbSc					= task->nmbSc;
	int subframe				= task->subframe;
	int taskid					= task->taskid;
	int nmbLayer				= task->nmbLayer;
	int userid					= task->userid;

	/**
	 * Pointer Chasing is quite ineffecient
	 * So directly use phy_vars instead
	 */
	MKL_Complex8 *deint_symbols		= phy_vars_p->deint_symbols;
	char *softbits					= phy_vars_p->softbits;
	float pow						= phy_vars_p->pow[0][0];

	int nmbSymbols  				= NMB_SLOT*nmbSc*(OFDM_IN_SLOT-1)*nmbLayer;
	
	clock_gettime(CLOCK_REALTIME,&demap_task_timer_start[subframe][userid][taskid]);
	// The Modulation Scheme is hard coded, Please Generalize 
	soft_demap_pthread1(deint_symbols, pow, MOD_64QAM, nmbSymbols, softbits, MAX_DEMAP_TASKS, taskid);
	
	clock_gettime(CLOCK_REALTIME,&demap_task_timer_stop[subframe][userid][taskid]);
	task->computed = true;
}

/* Optional tests to resolve DEMAP timing anomalies */
char softbits[2*2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS];
MKL_Complex8 in[2*(OFDM_IN_SLOT-1)*MAX_SC*MAX_LAYERS];
static void compute_demap_test(int argc, char **argv) {
	if(argc < 3) {
		fprintf(stderr,"Need a file to dump the output\n");
		exit(EXIT_FAILURE);
	}
	char *log_dir = argv[2];
	phy_task2_t *local_task = (phy_task2_t*)malloc(sizeof(phy_task2_t));

	struct timespec t1, t2;
	FILE *fd = fopen(strcat(log_dir,"/tmp_demap_test.csv"),"w");
	
	/* generate the user */
	gen_ue(88,100,4,MOD_64QAM,&ue);

	/* allocate the task values */
	local_task->task_type 	= DEMAP;
	local_task->layer		= 0;
	local_task->rx			= 0;
	local_task->startSc		= 0;
	local_task->nmbSc		= 1200;
	local_task->nmbLayer	= 4;
	local_task->mod			= MOD_64QAM; /* AVOID Hard coding it */
	
	local_task->subframe	= ue.subframe;
	local_task->userid		= ue.ueid;
	local_task->slotid		= 0;
	local_task->taskid		= 0;
	
	local_task->computed	= false;

	int nmbSymbols  		= NMB_SLOT*(local_task->nmbSc)*(OFDM_IN_SLOT-1)*(local_task->nmbLayer);

	int i = 0;
	float	pow			= phy_vars.pow[0][0];	
	
	fprintf(fd,"sample,time\n");
	for(i = 0; i < 3000; i++) {
		clock_gettime(CLOCK_REALTIME,&t1);
		compute_demap(local_task);
		clock_gettime(CLOCK_REALTIME,&t2);
		fprintf(fd,"%d,%lld\n",i,calculate_time_diff_spec(t2,t1));
	}
	fclose(fd);
}

static inline void handle_task(phy_task2_q_t *phy_tq, int thr_id) {
	phy_task2_t *task	= NULL;

	pthread_mutex_lock(&(phy_tq->lock));
	if(phy_tq->count > 0) {
		task			= phy_tq->first;
		phy_tq->first	= task->next;
		phy_tq->count--;
	}
	pthread_mutex_unlock(&(phy_tq->lock));

	/* Perform Computation */
	if(task) {
		switch(task->task_type) {
			case MICF : 
			{
				compute_micf(task);
				break;
			}
			case COMBWC :
			{
				compute_combwc(task);
				break;
			}
			case ANTCOMB :
			{
				compute_antcomb(task);
				break;
			}

			case DEMAP :
			{
				compute_demap(task);
				break;
			}
			default:
			{
				fprintf(stderr,"subframe@%d,user@%d,task@%d:Encounterd unsupported task type: %i\n",\
			  	 task->subframe, task->userid,task->taskid,task->task_type);
				exit(EXIT_FAILURE);
			}
		}
	}
}

/* Barrier */
static inline void wait_until_computed(phy_task2_t* array, int num) {
	int i = 0;
	
	while(i < num) {
		while(array[i].computed == false);
		i++;
	}
}

/* --------------------------------------------------------------------------*/
/* Producer-Consumer Synchronization Queue */
static phy_task2_q_t sync_q;

/* Array of generated tasks as place holder */
static phy_task2_t	  phy_task_array[MAX_TASKS];
/* --------------------------------------------------------------------------*/

/* Execute a phy task */
static void *exec_phy_task_ms(void *arg) {
	
	int *th_id = (int*)(arg);
	int thr_id = *th_id;
	
	affinity_set_cpu(thr_id+1);

	/* Look for task in your Task Queue and Execute */
	while(1)
		handle_task(&sync_q,thr_id);
}

static inline void init_globals(int affinity_offset) {
	/* affine */
	affinity_set_cpu(0+affinity_offset);
	
	/* FFT Handle Initialization */
	fft_init(MAX_SC,&global_fft_handle);
	
	/* Initialize the timing variables */
	struct timespec tmp;
	int subframe, user, slot, tid;
	clock_gettime(CLOCK_REALTIME,&tmp);
	for(subframe = 1; subframe < MAX_SUBFRAMES; subframe++) {
		for(user = 0; user < MAX_USERS; user++) {
			for(slot = 0; slot < NMB_SLOT; slot++) {
				for(tid = 0; tid < MAX_THREADS; tid++) {
					micf_task_timer_start[subframe][user][slot][tid] = tmp;
					micf_task_timer_stop[subframe][user][slot][tid]  = tmp;
					combwc_task_timer_start[subframe][user][slot][tid] = tmp;
					combwc_task_timer_stop[subframe][user][slot][tid]  = tmp;
					antcomb_task_timer_start[subframe][user][slot][tid] = tmp;
					antcomb_task_timer_stop[subframe][user][slot][tid]  = tmp;

					if(slot == 0) {
						demap_task_timer_start[subframe][user][tid] = tmp;
						demap_task_timer_stop[subframe][user][tid]  = tmp;
					}
				}
				micf_start[subframe][user][slot] = tmp;
				micf_stop[subframe][user][slot]  = tmp;
				combwc_start[subframe][user][slot]  = tmp;
				combwc_stop[subframe][user][slot]   = tmp;
				antcomb_start[subframe][user][slot] = tmp;
				antcomb_stop[subframe][user][slot]  = tmp;
				
				if(slot == 0) {
					demap_start[subframe][user] = tmp;
					demap_stop[subframe][user]  = tmp;
				}
			}
		}
	}
	/* Initialze a pregenerated data */
	gen_input_data(&pregen_data);

	/* Create a thread pool */
	int rc, i;
	for(i = 0; i < MAX_THREADS; i++) {
		th_id[i]			= i + affinity_offset;
		rc 					= \
		pthread_create(&th[i],NULL,exec_phy_task_ms,&th_id[i]);

		if(rc < 0) {
			fprintf(stderr,"Thread Creation failed \n");
			exit(EXIT_FAILURE);
		}
	}
}

static inline void deinit_globals(char *log_dir) {
	printf("Dumping Profile Data in %s directory\n", log_dir);
	/* Dump data into files */
	char dest1[100], dest2[100], dest3[100], dest4[100], dest5[100];
	strcpy(dest1,log_dir);
	strcpy(dest2,log_dir);
	strcpy(dest3,log_dir);
	strcpy(dest4,log_dir);
	strcpy(dest5,log_dir);
	strcat(dest1,"/micf_f.csv");
	strcat(dest2,"/combwc_f.csv");
	strcat(dest3,"/antcomb_f.csv");
	strcat(dest4,"/demap_f.csv");
	strcat(dest5,"/sf_f.csv");
	dump_micf(dest1);
	dump_combwc(dest2);
	dump_antcomb(dest3);
	dump_demap(dest4);
	dump_sf(dest5);

	/* Deinitialize the Global Variable */	
	fft_deinit(&global_fft_handle);
}

/* Note that comp_time is in us */
static inline long long diff_idx(long long comp_time, unsigned int delta) {
	return (1 + (comp_time / delta));
}

static inline long long diff_sleep(long long comp_time, unsigned int delta) {
	return (delta - (comp_time % delta));
}

/* Phy uplink with Master Slave Synchronization */
int phy_ms(int argc, char **argv) {
	if(argc < 5) {
		fprintf(stderr,"Required 4 arguments : RBs, log directory, affinity offset and odd-even-value\n");
		exit(EXIT_FAILURE);
	}
	
	/* Resolve the arguments */
	int nmbRB			= atoi(argv[1]);
	int nmbUsers		= atoi(argv[2]);
	char *log_dir 		= argv[3];
	int affinity_offset	= atoi(argv[4]);
	int odd_even_val	= atoi(argv[5]);

	/* Initializations */
	init_globals(affinity_offset);	
	printf("Started and Initialized ue \n");
	
	int subframe_count = 0;
	int rx, layer, sc, tile;
	int slot;
	int i;
	int startSc 	= 0;
	int nmbSc		= 0;
	int start_tile 	= 0;
	int end_tile   	= 0;
	int nmbLayer   	= 0;
	int nmbSymbols  = 0;
	int nmbSoftbits = 0;
	int ofdm, ofdm_count;
	int delta		= 2000;
	int tolerance	= 200;	/* Shouldn't be more than 10% of delta */
	long long comp_time;

	/* Generate subframes, each containing 1 ue */
	subframe_count = 0;
	
	while(subframe_count < MAX_SUBFRAMES) {
		/* Generate a ue with its inputs */
		gen_ue(subframe_count,nmbRB,4,MOD_64QAM,&ue);
		
		/* Wait for the alarm */
		/*		Implement	  */
			
		startSc  	= ue.start_rb * 12;
		nmbSc 	 	= ue.nmb_rb * 12;
		nmbLayer 	= ue.nmb_layer;
		nmbSymbols 	= NMB_SLOT * nmbSc * (OFDM_IN_SLOT - 1) * nmbLayer;	
		nmbSoftbits	= nmbSymbols * ue.mod;
	
		/** Timer Start */
		clock_gettime(CLOCK_REALTIME,&sf_start[subframe_count]);
		/* Do User Level Scheduling */
		for(slot = 0; slot < NMB_SLOT; slot++) {

			/* Create MICF Tasks */
			i = 0;
			/** Timer Start */
			clock_gettime(CLOCK_REALTIME,&micf_start[subframe_count][0][slot]);
			for(layer = 0; layer < ue.nmb_layer; layer++) {
				for(rx = 0; rx < RX_ANT; rx++) {
					phy_task_array[i].task_type = MICF;
					phy_task_array[i].layer		= layer;
					phy_task_array[i].rx		= rx;
					phy_task_array[i].startSc	= startSc;
					phy_task_array[i].nmbSc		= nmbSc;
					phy_task_array[i].nmbLayer	= nmbLayer;
				
					phy_task_array[i].computed	= false;

					phy_task_array[i].subframe	= ue.subframe;
					phy_task_array[i].userid	= ue.ueid;
					phy_task_array[i].slotid	= slot;
					phy_task_array[i].taskid	= i;
					
					if(i != 0)
						phy_task_array[i-1].next = &phy_task_array[i];
					i++;
				}
			}
			phy_task_array[i-1].next = NULL;

			pthread_mutex_lock(&sync_q.lock);
			if(sync_q.first == NULL)
				sync_q.first		= phy_task_array;
			else
				(sync_q.last)->next	= phy_task_array;
			sync_q.count	+= i;
			sync_q.last		 = &phy_task_array[i-1];
			pthread_mutex_unlock(&sync_q.lock);

			/* Try to handle the task yourself */
			while(sync_q.count)
				handle_task(&sync_q,-1);

			/* Barrier */
			wait_until_computed(phy_task_array, RX_ANT*(ue.nmb_layer));
			/* Timer End */
			clock_gettime(CLOCK_REALTIME,&micf_stop[subframe_count][0][slot]);

			i = 0;
			/* Timer Start */
			clock_gettime(CLOCK_REALTIME,&combwc_start[subframe_count][0][slot]);
			for(i = 0; i < MAX_COMBWC_TASKS; i++) {
					phy_task_array[i].task_type = COMBWC;
					phy_task_array[i].layer		= layer;
					phy_task_array[i].rx		= rx;
					phy_task_array[i].startSc	= startSc;
					phy_task_array[i].nmbSc		= nmbSc;
					phy_task_array[i].nmbLayer	= nmbLayer;
				
					phy_task_array[i].computed	= false;
					//phy_task_array[i].valid		= true;

					phy_task_array[i].subframe	= ue.subframe;
					phy_task_array[i].userid	= ue.ueid;
					phy_task_array[i].slotid	= slot;
					phy_task_array[i].taskid	= i;
					
					if(i != 0)
						phy_task_array[i-1].next = &phy_task_array[i];

			}
			phy_task_array[i-1].next = NULL;
			
			pthread_mutex_lock(&sync_q.lock);
			if(sync_q.first == NULL)
				sync_q.first		= phy_task_array;
			else
				(sync_q.last)->next	= phy_task_array;
			sync_q.count	+= i;
			sync_q.last		 = &phy_task_array[i-1];
			pthread_mutex_unlock(&sync_q.lock);

			/* Try to handle the task yourself */
			while(sync_q.count)
				handle_task(&sync_q,0);
			
			/* Barrier */
			wait_until_computed(phy_task_array, MAX_COMBWC_TASKS);
			clock_gettime(CLOCK_REALTIME,&combwc_stop[subframe_count][0][slot]);
			/* Interleave Before Antenna Combiner */
			for (rx = 0; rx < RX_ANT; rx++)
		  		for (layer = 0; layer < nmbLayer; layer++)
		    		for (sc = 0; sc < nmbSc; sc++)
		      			phy_vars.combWeight[layer][sc][rx] = \ 
						phy_vars.comb_w[sc][layer][rx];
		
			/* Create ANTCOMB task */
			i = 0;
			/* Timer Start */
			clock_gettime(CLOCK_REALTIME,&antcomb_start[subframe_count][0][slot]);
			for (layer=0; layer<nmbLayer; layer++) {
		  		int ofdm_count = 0;
		  			for (ofdm = 0; ofdm<OFDM_IN_SLOT; ofdm++) {
						if (ofdm != 3) {
							phy_task_array[i].task_type 	= ANTCOMB;
							phy_task_array[i].layer			= layer;
							phy_task_array[i].rx			= rx;
							phy_task_array[i].startSc		= startSc;
							phy_task_array[i].nmbSc			= nmbSc;
							phy_task_array[i].nmbLayer		= nmbLayer;
							phy_task_array[i].ofdm			= ofdm;
							phy_task_array[i].ofdm_count	= ofdm_count;
							
							phy_task_array[i].subframe	= ue.subframe;
							phy_task_array[i].userid	= ue.ueid;
							phy_task_array[i].slotid	= slot;
							phy_task_array[i].taskid	= i;
					
							phy_task_array[i].computed	= false;
							if(i != 0)
								phy_task_array[i-1].next = &phy_task_array[i];

							ofdm_count++;
							i++;	
						}
					}
			}
			phy_task_array[i-1].next = NULL;
			
			pthread_mutex_lock(&sync_q.lock);
			if(sync_q.first == NULL)
				sync_q.first		= phy_task_array;
			else
				(sync_q.last)->next	= phy_task_array;
			sync_q.count	+= i;
			sync_q.last		 = &phy_task_array[i-1];
			pthread_mutex_unlock(&sync_q.lock);

			/* Try to handle the task yourself */
			while(sync_q.count)
				handle_task(&sync_q,-1);
			
			/* Barrier */
			wait_until_computed(phy_task_array, i);
			clock_gettime(CLOCK_REALTIME,&antcomb_stop[subframe_count][0][slot]);
		}	/* SLOT LOOP */

		/* INTERLEAVE */
		interleave(phy_vars.symbols,phy_vars.deint_symbols,nmbSymbols);

		/* Create DEMAP task */
		i = 0;
		/* Timer Start */
		clock_gettime(CLOCK_REALTIME,&demap_start[subframe_count][0]);
		for (i = 0; i < MAX_DEMAP_TASKS; i++) {
			phy_task_array[i].task_type 	= DEMAP;
			phy_task_array[i].layer			= layer;
			phy_task_array[i].rx			= rx;
			phy_task_array[i].startSc		= startSc;
			phy_task_array[i].nmbSc			= nmbSc;
			phy_task_array[i].nmbLayer		= nmbLayer;
			//phy_task_array[i].mod			= MOD_64QAM; /* AVOID Hard coding it */
			
			phy_task_array[i].subframe	= ue.subframe;
			phy_task_array[i].userid	= ue.ueid;
			phy_task_array[i].slotid	= 0;			//slot;
			phy_task_array[i].taskid	= i;
			
			phy_task_array[i].computed	= false;
			if(i != 0)
				phy_task_array[i-1].next = &phy_task_array[i];
		}

		phy_task_array[i-1].next = NULL;
		
		pthread_mutex_lock(&sync_q.lock);
		if(sync_q.first == NULL)
			sync_q.first		= phy_task_array;
		else
			(sync_q.last)->next	= phy_task_array;
		sync_q.count	+= i;
		sync_q.last		 = &phy_task_array[i-1];
		pthread_mutex_unlock(&sync_q.lock);

		/* Try to handle the task yourself */
		while(sync_q.count)
			handle_task(&sync_q,-1);
		
		/* Barrier */
		wait_until_computed(phy_task_array, i);
		/** Timer End */
		clock_gettime(CLOCK_REALTIME,&demap_stop[subframe_count][0]);

		/* CRC and Turbo Decoder */
		turbo_dec(nmbSoftbits);
		crcFast(phy_vars.bits,nmbSoftbits/24);
		/** Timer End */
		clock_gettime(CLOCK_REALTIME,&sf_stop[subframe_count]);
		comp_time = (calculate_time_diff_spec(sf_stop[subframe_count],sf_start[subframe_count]))/1000;

		/* End Adjustment for timing syncronization */
		/* subframe_count update */
		subframe_count += diff_idx(comp_time,delta+tolerance);

		/* Sleep for a residual amount of time */
		usleep(diff_sleep(comp_time,delta+tolerance));
	}
	printf("Single ue simulation finished \n");
	deinit_globals(log_dir);
}
