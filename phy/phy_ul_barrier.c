#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <stdint.h>
#include <assert.h>
#include "kernel_def.h"
#include "common_utils.h"
#include "util.h"
#include "ant_comb_7.h"
#include "chest_5.h"
#include "complex_def.h"
#include "crc_13.h"
#include "fft_8.h"
#include "interleave_11.h"
#include "mf_4.h"
#include "soft_demap_9.h"
#include "turbo_dec_12.h"
#include "weight_calc_6.h"

//#define DEBUG_L0
#ifdef DEBUG_L0
static pthread_mutex_t print_lock;
#endif

/* Parallelized symbol demap */
/*
 * The Demap code in src/soft_demap_9.c gives
 * some timing anomalies. Even though
 * the code is exactly the same
 * The Most Likely Culprit is the IPO/Multifile
 * optimization by icc, please verify
 */
static inline void soft_demap_pthread1(MKL_Complex8 *in, float scaling_factor, int mod,\
						int n, char* out, int num_threads, int id) {
  	float L;
	int32_t 	 L_fixedpt;
	int16_t		 real_fixedpt, imag_fixedpt;
  	int i,k,ind;

    unsigned int work_per_thread = n/num_threads;

  switch (mod) {
      case MOD_64QAM:
    {
      //printf("demap-id:%d,executing 64qam\n",id);
      int32_t 	temp_real, temp_imag; 
      uint32_t 	temp_realu, temp_imagu; /* Unsigned Values */
      int32_t 	scaling_factor_fixedpt = (int32_t)scaling_factor;
      ind = 0;
      for (i = id*work_per_thread; i < (id+1)*work_per_thread; i++) {
    	ind = i*2*6;
    	for (k=0; k<6; k++) {
			/* Convert to fixed point */
      		L_fixedpt 		= k << 12; /* multiply by 4096 */
			real_fixedpt	= (int16_t)in[i].real;
			imag_fixedpt	= (int16_t)in[i].imag;
			
			/* Compute intermediate values */
      		temp_real = (((L_fixedpt - 1) - ((real_fixedpt * scaling_factor_fixedpt) >> 8) + (L_fixedpt - 1)) >> 1);
      		temp_imag = (((L_fixedpt - 1) - ((imag_fixedpt * scaling_factor_fixedpt) >> 8) + (L_fixedpt - 1)) >> 1);

			/* Restrict the values */
      		if (temp_real < 0)
      			temp_realu = 0;
      		else if (temp_real > (L_fixedpt - 1))
      			temp_realu = (L_fixedpt - 1);
      		if (temp_imag < 0)
      			temp_imagu = 0;
      		else if (temp_imag > (L_fixedpt - 1))
      			temp_imagu = (L_fixedpt - 1);

			/* Assign value to the output array */
      		out[ind++] = (temp_realu & 0x000000ff);
      		out[ind++] = (temp_imagu & 0x000000ff);
    	}
      }
    }
 
	break;

  default:
    fprintf(stderr,"(DEMAP) Modulation not supported: %i\n", mod);
  }
}

/* Execute a phy task */
void *exec_phy_task(void *arg) {
	
	int *th_id = (int*)(arg);
	int thr_id = *th_id;
 
	/* Core 0 contains code for generation of subframes */
	affinity_set_cpu(thr_id+1);

	/* meta data */
	int slot		= 0;	
	int layer 		= 0;
	int rx	  		= 0;
	int startSc 	= 0;
	int nmbSc		= 0;
	int start_tile 	= 0;
	int end_tile   	= 0;
	int nmbLayer   	= 0;
	int nmbSymbols  = 0;
	int nmbSoftbits = 0;
	MKL_Complex8* in[RX_ANT];
	int ofdm, ofdm_count;
	int index_out;
	int sc;

	/* timing variables */
	struct timespec start, stop;

	int i = 0;
	while(1) {
		/* Repeatedly look for computations allocated to it phy_task_t structure */
#ifdef DEBUG_L0
		//pthread_mutex_lock(&print_lock);
		printf("Thr@%d: Waiting on b2\n",thr_id);
		//pthread_mutex_lock(&print_lock);
#endif
		pthread_barrier_wait(&b2);
		startSc  	= ue.start_rb * 12;
		nmbSc 	 	= ue.nmb_rb * 12;
		nmbLayer 	= ue.nmb_layer;
		nmbSymbols 	= NMB_SLOT * nmbSc * (OFDM_IN_SLOT - 1) * nmbLayer;	
		nmbSoftbits	= nmbSymbols * ue.mod;
	
		for(slot = 0; slot < 2; slot++) {
			/* Compute MICF (phase 1) */
			pthread_barrier_wait(&b);
			if(thr_id == 1)
				clock_gettime(CLOCK_REALTIME,&micf_start[i][0][slot]);
			clock_gettime(CLOCK_REALTIME,&micf_task_timer_start[i][0][slot][thr_id]);
			if(thr_id < nmbLayer * RX_ANT) {
				/* Mapping : Thread-Id -> (layer, rx) */
				rx 		= thr_id % RX_ANT;
				layer 	= thr_id / RX_ANT;
				/* Matched Filter */
				mf(&(ue.data.in_data[slot][3][rx][startSc]), 	 \
				   &(ue.data.in_rs[slot][startSc][layer]), /* Check the indices*/ \
				   nmbSc, phy_vars.layer_data[layer][rx], 		 \
				   &(phy_vars.pow[layer][rx]));
				/* IFFT */
				ifft(phy_vars.layer_data[layer][rx],			\
					 nmbSc,										\
					 ue.data.fft_tf[slot],						\
					 &global_fft_handle);
				/* Windowing */
				window(phy_vars.layer_data[layer][rx],			\
					  phy_vars.pow[layer][rx],					\
					  nmbSc,									\
					  phy_vars.layer_data[layer][rx],			\
					  &(phy_vars.res_power[layer][rx]));
				/* Intermediate Data Storage */
				//phy_vars.R[layer][rx] = 0.0 + 0.0 * I;//cmake(phy_vars.res_power[layer][rx],0);
				phy_vars.R[layer][rx].real = 0.0;//cmake(phy_vars.res_power[layer][rx],0);
				phy_vars.R[layer][rx].imag = 0.0;//cmake(phy_vars.res_power[layer][rx],0);
				/* FFT */
				fft(phy_vars.layer_data[layer][rx],nmbSc,ue.data.fft_tf[slot],&global_fft_handle);
			}
			clock_gettime(CLOCK_REALTIME,&micf_task_timer_stop[i][0][slot][thr_id]);
			pthread_barrier_wait(&b);
			if(thr_id == 1)
				clock_gettime(CLOCK_REALTIME,&micf_stop[i][0][slot]);

			/* Compute COMBWC (phase 2) */
			if(thr_id == 1)
				clock_gettime(CLOCK_REALTIME,&combwc_start[i][0][slot]);
			clock_gettime(CLOCK_REALTIME,&combwc_task_timer_start[i][0][slot][thr_id]);
			if(thr_id < MAX_COMBWC_TASKS) {
				start_tile = (thr_id)*(nmbSc/MAX_COMBWC_TASKS);
				end_tile   = (thr_id+1)*(nmbSc/MAX_COMBWC_TASKS);
				comb_w_calc_partiled(phy_vars.layer_data,start_tile,end_tile,nmbLayer,phy_vars.R,phy_vars.comb_w);
			}
			clock_gettime(CLOCK_REALTIME,&combwc_task_timer_stop[i][0][slot][thr_id]);
			pthread_barrier_wait(&b);
			if(thr_id == 1)
				clock_gettime(CLOCK_REALTIME,&combwc_stop[i][0][slot]);
			
			/* Reshuffling */
			if(thr_id == 0) {
        	for (rx=0; rx<RX_ANT; rx++)
          		for (layer=0; layer<ue.nmb_layer; layer++)
	    			for (sc=0; sc<nmbSc; sc++)
	      				phy_vars.combWeight[layer][sc][rx] = \
						phy_vars.comb_w[sc][layer][rx];
			}
			pthread_barrier_wait(&b);
			
			/* Antenna Combiner (phase 3) */
			if(thr_id == 1)
				clock_gettime(CLOCK_REALTIME,&antcomb_start[i][0][slot]);
			clock_gettime(CLOCK_REALTIME,&antcomb_task_timer_start[i][0][slot][thr_id]);
			if(thr_id < nmbLayer*(OFDM_IN_SLOT-1)) {
				ofdm_count 	= thr_id % (OFDM_IN_SLOT-1);
				layer		= thr_id / (OFDM_IN_SLOT-1);
				ofdm 		= (ofdm_count > 2)?(ofdm_count+1):ofdm_count;
				for(rx = 0; rx < RX_ANT; rx++)
					in[rx] = &ue.data.in_data[slot][ofdm][rx][startSc];

				index_out = nmbSc*(ofdm_count + slot*(OFDM_IN_SLOT-1) + \
							layer*NMB_SLOT*(OFDM_IN_SLOT-1));

				ant_comb(in,phy_vars.combWeight[layer],nmbSc,&phy_vars.symbols[index_out]);
				ifft(&phy_vars.symbols[index_out],nmbSc,ue.data.fft_tf[slot],&global_fft_handle);
			}
			clock_gettime(CLOCK_REALTIME,&antcomb_task_timer_stop[i][0][slot][thr_id]);
			pthread_barrier_wait(&b);
			if(thr_id == 1)
				clock_gettime(CLOCK_REALTIME,&antcomb_stop[i][0][slot]);
		}

		/* Interleave */
		if(thr_id == 1)
			interleave(phy_vars.symbols,phy_vars.deint_symbols,nmbSymbols);		
		
		/* Soft Symbol Demap */
		if(thr_id == 1)
			clock_gettime(CLOCK_REALTIME,&demap_start[i][0]);
		pthread_barrier_wait(&b);
		clock_gettime(CLOCK_REALTIME,&demap_task_timer_start[i][0][thr_id]);
		if(thr_id < MAX_DEMAP_TASKS)
			soft_demap_pthread1(phy_vars.deint_symbols,phy_vars.pow[0][0],ue.mod,nmbSymbols,phy_vars.softbits,MAX_DEMAP_TASKS,thr_id);
		clock_gettime(CLOCK_REALTIME,&demap_task_timer_stop[i][0][thr_id]);
		pthread_barrier_wait(&b);
		if(thr_id == 1)
			clock_gettime(CLOCK_REALTIME,&demap_stop[i][0]);

		/* CRC and Turbo Decoder */
		turbo_dec(nmbSoftbits);
		crcFast(phy_vars.bits,nmbSoftbits/24);
		pthread_barrier_wait(&b2);	

		i++;
	}
}

static inline void init_globals(int affinity_offset) {
	/* affine */
	affinity_set_cpu(0+affinity_offset);
	
	/* FFT Handle Initialization */
	fft_init(MAX_SC,&global_fft_handle);
	
	/* Initialize the barrier */
	pthread_barrier_init(&b,NULL,MAX_THREADS);
	pthread_barrier_init(&b2,NULL,MAX_THREADS+1);

	/* Initialize the timing variables */
	struct timespec tmp;
	int subframe, user, slot, tid;
	clock_gettime(CLOCK_REALTIME,&tmp);
	for(subframe = 1; subframe < MAX_SUBFRAMES; subframe++) {
		for(user = 0; user < MAX_USERS; user++) {
			for(slot = 0; slot < NMB_SLOT; slot++) {
				for(tid = 0; tid < MAX_THREADS; tid++) {
					micf_task_timer_start[subframe][user][slot][tid] = tmp;
					micf_task_timer_stop[subframe][user][slot][tid]  = tmp;
					combwc_task_timer_start[subframe][user][slot][tid] = tmp;
					combwc_task_timer_stop[subframe][user][slot][tid]  = tmp;
					antcomb_task_timer_start[subframe][user][slot][tid] = tmp;
					antcomb_task_timer_stop[subframe][user][slot][tid]  = tmp;

					if(slot == 0) {
						demap_task_timer_start[subframe][user][tid] = tmp;
						demap_task_timer_stop[subframe][user][tid]  = tmp;
					}
				}
				micf_start[subframe][user][slot] = tmp;
				micf_stop[subframe][user][slot]  = tmp;
				combwc_start[subframe][user][slot]  = tmp;
				combwc_stop[subframe][user][slot]   = tmp;
				antcomb_start[subframe][user][slot] = tmp;
				antcomb_stop[subframe][user][slot]  = tmp;
				
				if(slot == 0) {
					demap_start[subframe][user] = tmp;
					demap_stop[subframe][user]  = tmp;
				}
			}
		}
	}
	/* Initialze a pregenerated data */
	gen_input_data(&pregen_data);

	/* Create a thread pool */
	int rc, i;
	for(i = 0; i < MAX_THREADS; i++) {
		th_id[i]			= i + affinity_offset;
		rc 					= \
		pthread_create(&th[i],NULL,exec_phy_task,&th_id[i]);

		if(rc < 0) {
			fprintf(stderr,"Thread Creation failed \n");
			exit(EXIT_FAILURE);
		}
	}
}

static inline void deinit_globals(char *log_dir) {
	printf("Dumping Profile Data in %s directory\n", log_dir);
	/* Dump data into files */
	char dest1[100], dest2[100], dest3[100], dest4[100], dest5[100];
	strcpy(dest1,log_dir);
	strcpy(dest2,log_dir);
	strcpy(dest3,log_dir);
	strcpy(dest4,log_dir);
	strcpy(dest5,log_dir);
	strcat(dest1,"/micf_f.csv");
	strcat(dest2,"/combwc_f.csv");
	strcat(dest3,"/antcomb_f.csv");
	strcat(dest4,"/demap_f.csv");
	strcat(dest5,"/sf_f.csv");
	dump_micf(dest1);
	dump_combwc(dest2);
	dump_antcomb(dest3);
	dump_demap(dest4);
	dump_sf(dest5);

	/* Deinitialize the Global Variable */	
	fft_deinit(&global_fft_handle);
}


int phy(int argc, char **argv) {
	if(argc < 5) {
		fprintf(stderr,"Required 4 arguments : RBs, log directory, affinity offset and odd-even-value\n");
		exit(EXIT_FAILURE);
	}
	
	/* Resolve the arguments */
	int nmbRB			= atoi(argv[1]);
	char *log_dir 		= argv[2];
	int affinity_offset	= atoi(argv[3]);
	int odd_even_val	= atoi(argv[4]);

	assert(affinity_offset == 0);	/* This code is not meant for pipelined SF execution, affinity offset is useless here */
	
	int subframe_count	= 0;

	/* Initializations */
	init_globals(affinity_offset);	
	
	/* Generate subframes, each containing 1 ue */
	subframe_count = 0;
	while(subframe_count < MAX_SUBFRAMES) {
		/* Generate a ue with its inputs */
		gen_ue(subframe_count,nmbRB,4,MOD_64QAM,&ue);
#ifdef DEBUG_L0
		//pthread_mutex_lock(&print_lock);
		printf("sf(%d) generated, waiting on b2\n",subframe_count);
		//pthread_mutex_unlock(&print_lock);
#endif
		pthread_barrier_wait(&b2);
		clock_gettime(CLOCK_REALTIME,&sf_start[subframe_count]);
#ifdef DEBUG_L0
		//pthread_mutex_lock(&print_lock);
		printf("sf(%d) waiting to finish on b2\n",subframe_count);
		//pthread_mutex_unlock(&print_lock);
#endif
		pthread_barrier_wait(&b2);
		clock_gettime(CLOCK_REALTIME,&sf_stop[subframe_count]);
		subframe_count++;
	}

	/* dump the contents */
	deinit_globals(log_dir);
}
