#ifndef UTIL_DEF_H
#define UTIL_DEF_H

#include <complex.h>
#include "mkl_types.h"

#define MAX_SUBFRAMES 3000
#define MAX_USERS 	  10
#define MAX_THREADS   24		/* The number of slave threads */
#define MAX_COMBWC_TASKS 12
#define MAX_DEMAP_TASKS  24
#define MAX_TASKS		32
#define NUMCORES	 256

typedef mod_type mod_t;

typedef enum {
	true 	= 1,
	false 	= 0
} bool;

/* Input Data Structure */
typedef struct __input_data {
	MKL_Complex8 in_data[NMB_SLOT][OFDM_IN_SLOT][RX_ANT][MAX_SC];
	MKL_Complex8 in_rs[NMB_SLOT][MAX_SC][MAX_LAYERS];

	/* Twiddle Factors : Why should it be an external input */
	MKL_Complex8 fft_tf[NMB_SLOT][MAX_SC];
} input_data_t;

/* User with its input and other meta data */
typedef struct __ue {
	/* Meta Data */
	int 	subframe;
	int 	start_rb;
	int 	nmb_rb;
	int 	nmb_layer;
	int 	ueid;
	bool 	last;
	mod_t	mod;

	/* Actual Input */
	input_data_t data;

	/* Check the validity */
	bool valid;
} ue_t;

void gen_input_data(input_data_t *in);

/* generate a ue with a given subframe id */
void gen_ue(int subframe_id,  \
			 int nmb_rb,	  \
			 int nmb_layer,	  \
			 mod_t mod,		  \
			 ue_t *ue);

/* generate a sf: modelled as an array of users */
void gen_sf(int subframe_id,  \
			 int nmb_rb,	  \
			 int nmb_layer,	  \
			 mod_t mod,		  \
			 ue_t *ue,		  \
			 int size);

void affinity_set_cpu(int _id);

/* "stack" variables for computation */
typedef struct __phy_vars {
	/* place holders for computation */
	scData_t 		layer_data[MAX_LAYERS][RX_ANT];
	float 			pow[MAX_LAYERS][RX_ANT];
	float 			res_power[MAX_LAYERS][RX_ANT];
	complexMatrix_t	R;
	complexMatrix_t comb_w[MAX_SC];
	weightSC_t		combWeight[MAX_LAYERS];
	MKL_Complex8	symbols[NMB_SLOT*(OFDM_IN_SLOT-1)*MAX_SC*MAX_LAYERS];
	MKL_Complex8	deint_symbols[NMB_SLOT*(OFDM_IN_SLOT-1)*MAX_SC*MAX_LAYERS];
	char			softbits[2*2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS];
	char			bits[2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS];

	mod_t			mod;
} phy_vars_t;

typedef enum {
	MICF 	= 0,
	COMBWC	= 1,
	ANTCOMB	= 2,
	DEMAP	= 3
} task_type_t;

typedef struct __phy_task2_t {
	/* Meta info */
	int 			layer;
  	int 			nmbLayer;
	int 			rx;
	int 			ofdm;
	int 			ofdm_count;
	int 			startSc;
	int				endSc;
	int 			nmbSc;
	mod_type 		mod;
	task_type_t		task_type;

	/* Task status */
	volatile bool 	computed;
	
	/* Ids to completely identified a task */
	unsigned int 	subframe;
	unsigned int 	userid;
	unsigned int 	taskid;
	unsigned int 	slotid;

	/* Linked List */
	struct __phy_task2_t *next;
} phy_task2_t;

typedef struct __phy_task2_q_t {
	pthread_mutex_t lock;
	phy_task2_t		*first;
	phy_task2_t		*last;
	volatile int	count;	
} phy_task2_q_t;

/* MS barrier */
//void wait_until_computed(phy_task2_t*, int);

/* ---------------------------------- Global Variables ----------------------*/
/**
 * For Communication and synchronization
 * with multiple threads
 */
pthread_barrier_t 	b;
pthread_barrier_t 	b2;

/* For Single UE case */
ue_t 				ue;
phy_vars_t 			phy_vars;

/* Thread Information */
pthread_t			th[MAX_THREADS];
int					th_id[MAX_THREADS];
void				*exec_phy_task_ms(void*);	/* thread start fn */

/* pregenerated Data to avoid generation time */
input_data_t		pregen_data;

/* Additional Structures for the multi-UE case */
phy_vars_t			phy_vars_mue[MAX_USERS];
ue_t				mue[MAX_USERS];
/* --------------------------------------------------------------------------*/

/* ------------------ Global Variables for timing instrumentation -----------*/
struct timespec micf_start[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT];
struct timespec micf_stop[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT];
struct timespec micf_task_timer_start[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT][MAX_THREADS];
struct timespec micf_task_timer_stop[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT][MAX_THREADS];

struct timespec combwc_start[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT];
struct timespec combwc_stop[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT];
struct timespec combwc_task_timer_start[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT][MAX_THREADS];
struct timespec combwc_task_timer_stop[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT][MAX_THREADS];

struct timespec antcomb_start[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT];
struct timespec antcomb_stop[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT];
struct timespec antcomb_task_timer_start[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT][MAX_THREADS];
struct timespec antcomb_task_timer_stop[MAX_SUBFRAMES][MAX_USERS][NMB_SLOT][MAX_THREADS];

struct timespec demap_start[MAX_SUBFRAMES][MAX_USERS];
struct timespec demap_stop[MAX_SUBFRAMES][MAX_USERS];
struct timespec demap_task_timer_start[MAX_SUBFRAMES][MAX_USERS][MAX_THREADS];
struct timespec demap_task_timer_stop[MAX_SUBFRAMES][MAX_USERS][MAX_THREADS];

struct timespec sf_start[MAX_SUBFRAMES];
struct timespec sf_stop[MAX_SUBFRAMES];
/* --------------------------------------------------------------------------*/

/* FFT handle for FFT initialization */
DFTI_DESCRIPTOR_HANDLE global_fft_handle;

/* ----------------------------Timing Dump-----------------------------------*/
void dump_micf(char *);
void dump_combwc(char *);
void dump_antcomb(char *);
void dump_demap(char *);
void dump_sf(char *);
/* --------------------------------------------------------------------------*/
#endif
