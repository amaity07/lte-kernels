/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/

#ifndef _MRC_H
#define _MRC_H

#include "kernel_def.h"
#include <complex.h>

void ant_comb(MKL_Complex8* in[4], weight_t* w, int n, MKL_Complex8* out);
//void ant_comb(complex *in, complex *antw, int n, complex *out);
void ant_comb_test(int nmbRB);
#endif
