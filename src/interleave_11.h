/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/

#ifndef _INTERLEAVE_H
#define _INTERLEAVE_H

//#include "complex_def.h"
#include "mkl.h"

//void interleave(complex* in, complex* out, int n);
void interleave(MKL_Complex8 *in, MKL_Complex8 *out, int n);

#endif
