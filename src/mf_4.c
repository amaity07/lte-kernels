/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/

#include "common_utils.h"
#include "mf_4.h"
#include "kernel_def.h"
#include "mkl.h"
#include "mkl_vml.h"

void mf(const MKL_Complex8 *in, const MKL_Complex8 *rs, const MKL_INT n, \
		MKL_Complex8 *out, float *power) {
 
	int i;
	float nrm;

	/* Calc power (conjugated dot product of in vector) */
	//for ( i = 0; i < n; i++ )
	//  ptot += in[i].re * in[i].re + in[i].im * in[i].im;
	//nrm = cblas_scnrm2(n, in, 1);
	//*power = nrm*nrm;
	cblas_cdotc_sub(n,in,1,in,1,power);
	
	/* Calc Matched Filter (Implemented as vector mathematical functions) */
	//for ( i = 0; i < n; i++ )
	//  out[i] = cmul(in[i], rs[i]);	
	//*power = ptot;
	vcMul(n,in,rs,out);	
}

#ifdef TEST_KERN
void mf_test(int nmbRB) {
	FILE* fd 	 = fopen("log/mf_test_ph1.csv","w");
	if(!fd) {
		fprintf(stderr,"Unable to open file\n");
		exit(EXIT_FAILURE);
	}
	float power 	 = 0;
	int nmbSc 	 = nmbRB*12;
	int i;
	MKL_Complex8 vector_in1[MAX_SC];
	MKL_Complex8 vector_in2[MAX_SC];
	MKL_Complex8 vector_out[MAX_SC];
	gen_vector(vector_in1,nmbSc);
	gen_vector(vector_in2,nmbSc);
	
	struct timespec start, end;
	long long time_diff;

	/* Take a sample of N readings */
	fprintf(fd,"#mf-sample(RB=%d),time(in ns)\n",nmbRB);
	for(i = 0; i < SAMPLES; i++) {
		clock_gettime(CLOCK_MONOTONIC,&start);
		mf(vector_in1,vector_in2,nmbSc,vector_out,&power);
		clock_gettime(CLOCK_MONOTONIC,&end);

		time_diff = calculate_time_diff_spec(end,start);
		fprintf(fd,"sample-%d,%lld\n",i,time_diff);
	}

	fclose(fd);
	printf("Number of Threads used in %d\n",mkl_get_max_threads());	
	//printf("Time Consumed (in ns) = %lld\n",time_diff);
}
#endif
