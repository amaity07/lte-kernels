/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/

#include <complex.h>
#include "chest_5.h"
#include "common_utils.h"
#include "kernel_def.h"
#include "mkl.h"
#include "mkl_vml.h"
#include "mkl_cblas.h"

#define TAP_FAC	4

//void chest(complex* in, int pow, int n, complex* out, int* res_power) {
//  int i;
//  long int ptot = 0;
//  int used_sc = n/TAP_FAC;    // n will always be >= 12 and possible to divide by 4
//
//  // Calc used power
//  for (i=0; i<used_sc; i++)
//    ptot += in[i].re * in[i].re + in[i].im * in[i].im;
//
//  ptot = ptot >> 16;
//  *res_power = pow - ptot;
//
//  if (*res_power < 0) 
//    *res_power = 1;
//
//  // Copy the used taps-coeffs
//  for (i=0; i<used_sc; i++)
//    out[i] = in[i];
//
//  // Reset the rest of the coefficients
//  for (i=used_sc; i<n; i++) {
//    out[i].re = 0;
//    out[i].im = 0;
//  }
//}

void window(MKL_Complex8 *in, float pow, int n, \
			MKL_Complex8 *out, float *res_power) {
	int i;
	int used_sc = n/TAP_FAC;
	float ptot;
	
	//MKL_Complex8 *rec_window = (MKL_Complex8*)mkl_calloc(n,sizeof(MKL_Complex8),64);

	/* Create the rectangular window vector */
	for(i = 0; i < n; i++) {
		if(i < used_sc) {
			//rec_window[i].real = 1.0;
			//rec_window[i].imag = 0.0;
			out[i] = in[i];
		}
		else	{
			//rec_window[i].real = 0.0;
			//rec_window[i].imag = 0.0;
			out[i].real = 0.0;
			out[i].imag = 0.0;
		}
	}

	/* Perform Windowing */
	//vcMul(n,in,rec_window,out);

	/* Compute the norm */
	//nrm = cblas_scnrm2(n,out,1);
	//*res_power = nrm*nrm;
	cblas_cdotc_sub(n,in,1,in,1,&ptot);

	ptot = ptot / (65536.0);
	*res_power = pow - ptot;
	
	if (*res_power < 0.0) 
	*res_power = 1.0;

	//mkl_free(rec_window);
}

#ifdef TEST_KERN
/* Timming Analysis */
void window_test(int nmbRB) {
	FILE* fd 	 = fopen("log/chest_test_ph1.csv","w");
	if(!fd) {
		fprintf(stderr,"Unable to open file\n");
		exit(EXIT_FAILURE);
	}
	int nmbSc 	  = nmbRB * 12;
	int pow 	  = 10;
	float res_power = 0.0;
	int i;

	struct timespec start, end;
	long long time_diff;
	
	MKL_Complex8 vector_in[MAX_SC];
	MKL_Complex8 vector_out[MAX_SC];
	gen_vector(vector_in, nmbSc);
	gen_vector(vector_out, nmbSc);

	fprintf(fd,"#chest-sample(RB=%d),time(in ns)\n",nmbRB);
	for(i = 0; i < SAMPLES; i++) {
		clock_gettime(CLOCK_MONOTONIC,&start);
		window(vector_in, pow, nmbSc, vector_out, &res_power);
		clock_gettime(CLOCK_MONOTONIC,&end);
		
		time_diff = calculate_time_diff_spec(end,start);
		fprintf(fd,"sample-%d,%lld\n",i,time_diff);
	}
	fclose(fd);	
}
#endif
