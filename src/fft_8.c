/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 *******************************************************************************
 * Note: This is not a correct FFT/IFFT, but it has approximatly the           *
 *       same number of operations as a proper implementation.                 *
 ******************************************************************************/

#include "common_utils.h"
#include "fft_8.h"
#include "stdlib.h"
#include "kernel_def.h"
#include "mkl_dfti.h"
#include "mkl.h"
#include "mkl_service.h"

/* Test for a null pointer in a chunk of contiguous words */
inline void test_ptr(const MKL_Complex8 *in, unsigned int N) {
	int i = 0;
	for(i = 0; i < N; i++) {
		if(in == NULL) {
			fprintf(stderr,"NULL pointer found in contiguous allocation\n");
			exit(EXIT_FAILURE);
		}
		in++;
	}
}

void fft_init(int n, DFTI_DESCRIPTOR_HANDLE *hand_p) {
	//long long time_diff;
	
	*hand_p = 0;
	
	MKL_LONG status = 0;
	
	status = mkl_enable_instructions(MKL_ENABLE_AVX512);

	if(status < 0) {
		fprintf(stderr,"Vectorization disable failed\n");
		exit(EXIT_FAILURE);
	}

	status = DftiCreateDescriptor(hand_p, DFTI_SINGLE, DFTI_COMPLEX, 1, (MKL_LONG)n);

	status = \
	DftiSetValue(*hand_p, DFTI_THREAD_LIMIT, 1);

	if (status != 0) {
		fprintf(stderr,"FFT setup failed\n");
		exit(EXIT_FAILURE);
	}

	status = \
	DftiCommitDescriptor(*hand_p);

	if (status != 0) {
		fprintf(stderr,"TF computation failed\n");
		exit(EXIT_FAILURE);
	}
}

void fft_deinit(DFTI_DESCRIPTOR_HANDLE *hand_p) {
	DftiFreeDescriptor(hand_p);
}

#define TEST_ALLOC

long long fft(MKL_Complex8 *io, int n, MKL_Complex8 *tf, DFTI_DESCRIPTOR_HANDLE *hand_p) {
#ifdef TEST_ALLOC
	test_ptr(io,n);
#endif
	DftiComputeForward(*hand_p, io);
	return 0;
}

//void ifft(MKL_Complex8 *io, int n, MKL_Complex8 *tf) {
//	DFTI_DESCRIPTOR_HANDLE hand = 0;
//	
//	MKL_LONG status = \
//	DftiCreateDescriptor(&hand, DFTI_SINGLE, DFTI_COMPLEX, 1, (MKL_LONG)n);
//
//	status = \
//	DftiSetValue(hand, DFTI_THREAD_LIMIT, 1);
//
//	if (status != 0) {
//		fprintf(stderr,"FFT setup failed\n");
//		exit(EXIT_FAILURE);
//	}
//
//	status = \
//	DftiCommitDescriptor(hand);
//
//	if (status != 0) {
//		fprintf(stderr,"TF computation failed\n");
//		exit(EXIT_FAILURE);
//	}
//
//	status = \
//	DftiComputeBackward(hand, io);
//	DftiFreeDescriptor(&hand);
//}

// For now just call fft to compute ifft
void ifft(MKL_Complex8 *io, int n, MKL_Complex8* tf, DFTI_DESCRIPTOR_HANDLE *hand_p) {
	fft(io,n,tf,hand_p);
}

#ifdef TEST_KERN
DFTI_DESCRIPTOR_HANDLE global_fft_hand = 0;		/* Global FFT handle */

void fft_test(int nmbRB) {
	FILE* fd 	 = fopen("log/fft_test_ph1.csv","w");
	int nmbSc = nmbRB * 12;
	struct timespec start, end;
	long long time_diff;
	int i;	
	MKL_Complex8 vector_io[MAX_SC];
	MKL_Complex8 vector_w[MAX_SC];
	gen_vector(vector_io,nmbSc);
	gen_vector(vector_w,nmbSc);
	fft_init(nmbSc,&global_fft_hand);

	fprintf(fd,"#fft-sample(RB=%d),time(in ns)\n",nmbRB);
	for(i = 0; i < SAMPLES; i++) {
		clock_gettime(CLOCK_MONOTONIC,&start);
		fft(vector_io,nmbSc,vector_w,&global_fft_hand);
		clock_gettime(CLOCK_MONOTONIC,&end);

		time_diff = calculate_time_diff_spec(end,start);
		fprintf(fd,"sample-%d,%lld\n",i,time_diff);
	}
	fft_deinit(&global_fft_hand);
	fclose(fd);	
	//printf("Time Consumed (in ns) = %lld\n",time_diff);
}
#endif
