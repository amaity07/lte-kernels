/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/
#ifndef _FFT_H
#define _FFT_H

#include "complex_def.h"
#include "mkl_dfti.h"
#include "mkl.h"
#include "mkl_service.h"

//void fft(MKL_Complex8 *io, int n, MKL_Complex8 *w);
//void ifft(MKL_Complex8 *io, int n, MKL_Complex8 *w);

long long fft(MKL_Complex8 *io, int n, MKL_Complex8* tf, DFTI_DESCRIPTOR_HANDLE *hand_p);	// hand_p is read only
void ifft(MKL_Complex8 *io, int n, MKL_Complex8* tf, DFTI_DESCRIPTOR_HANDLE *hand_p);
void fft_init(int n, DFTI_DESCRIPTOR_HANDLE *hand_p);		/* create a plan for n point FFT */
void fft_deinit(DFTI_DESCRIPTOR_HANDLE *hand_p);		/* create a plan for n point FFT */
void fft_test(int nmbRB);
#endif
