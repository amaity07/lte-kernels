/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/

#include "ant_comb_7.h"
#include "common_utils.h"
#include <complex.h>
#include "mkl.h"

#ifndef RX_ANT
#define RX_ANT 4
#endif

inline MKL_Complex8 __cmul(MKL_Complex8 a,MKL_Complex8 b) {
	MKL_Complex8 t;
	t.real = a.real * b.real - a.imag * b.imag;
	t.imag = a.imag * b.real + a.real * b.imag;
	return t;
}


inline MKL_Complex8 __cadd(MKL_Complex8 a,MKL_Complex8 b)	{
	MKL_Complex8 t;
	t.real = a.real + b.real;
	t.imag = a.imag + b.imag;
	return t;
}

void ant_comb(MKL_Complex8 *in[4], weight_t* w, int n, MKL_Complex8 *out) {
  
	MKL_Complex8 a0, a1, a2, a3, a01, a23;
	int i;

	struct timespec start, end;
	long long time_diff;

	/* Calc the weighted combination of the input data */
	for ( i = 0; i < n; i++ ) {
		a0 		= __cmul(in[0][i], w[i][0]);
		a1 		= __cmul(in[1][i], w[i][1]);
		a2 		= __cmul(in[2][i], w[i][2]);
		a3 		= __cmul(in[3][i], w[i][3]);
		a01 	= __cadd(a0, a1);
		a23 	= __cadd(a2, a3);
		out[i] 	= __cadd(a01, a23);
	}
}

#ifdef TEST_KERN
void ant_comb_test(int nmbRB) {
	int nmbSc = nmbRB * 12;
	
	int i, j, k, idx = 0;
	FILE* fd 	 = fopen("log/antcomb_ph3.csv","w");
	if(!fd) {
		fprintf(stderr,"Unable to open file\n");
		exit(EXIT_FAILURE);
	}

	/* Declare Array Size */
	MKL_Complex8 	*w;			/* Size : (RX_ANT*nmbSc) */
	weight_t 		*w1; 		/* Size : (RX_ANT*nmbSc) */
	MKL_Complex8 	*in; 		//[MAX_SC*RX_ANT];
	MKL_Complex8 	*in1[4]; 	//[MAX_SC*RX_ANT];
	MKL_Complex8 	*out; 		//[MAX_SC];

	/* Allocate Memory space */
	w 	= (MKL_Complex8 *)mkl_calloc(RX_ANT*nmbSc,sizeof(MKL_Complex8),64);	
	w1 	= (weight_t *)mkl_calloc(nmbSc,sizeof(weight_t),64);	
	in  = (MKL_Complex8 *)mkl_calloc(RX_ANT*nmbSc,sizeof(MKL_Complex8),64);	
	out = (MKL_Complex8 *)mkl_calloc(nmbSc,sizeof(MKL_Complex8),64);

	/* Initialize the inputs */
	gen_vector(w,RX_ANT*nmbSc);
	gen_vector(in,RX_ANT*nmbSc);

	/* Reshuffle the input format */
	idx = 0;	
	for(i = 0; i < RX_ANT; i++) {
		in1[i] = (MKL_Complex8 *)mkl_calloc(nmbSc,sizeof(MKL_Complex8),64);
		for(j = 0; j < nmbSc; j++) {
			in1[i][j] = in[idx++];
		}
	}

	for(i = 0; i < nmbSc; i++) {
		for(j = 0; j < RX_ANT; j++) {
			w1[i][j] = w[idx++];
		}
	}

	struct timespec start, end;
	long long time_diff;
	
	fprintf(fd,"#antcomb-sample(RB=%d),time(in ns)\n",nmbRB);
	for(i = 0; i < SAMPLES; i++) {
		clock_gettime(CLOCK_REALTIME,&start);
		ant_comb(in1, w1, nmbSc, out);
		clock_gettime(CLOCK_REALTIME,&end);
		time_diff = calculate_time_diff_spec(end,start);
		fprintf(fd,"sample-%d,%lld\n",i,time_diff);
	}
	fclose(fd);

	mkl_free(in);
	for(i = 0; i < RX_ANT; i++)
		mkl_free(in1[i]);
	mkl_free(w);	
	mkl_free(w1);
	mkl_free(out);

	printf("Ending antcomb with #%d, threads\n",mkl_get_max_threads());
}
#endif
