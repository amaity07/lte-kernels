/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/

#ifndef _KERNEL_DEF_H
#define _KERNEL_DEF_H

#ifdef TEST_KERN
#define MAX_SC 1200
#define RX_ANT 4
#define MAX_LAYERS 4
#define MAX_RB 100
#define NMB_SLOT 2
#define OFDM_IN_SLOT 7
#define SAMPLES 3000
#define BETA 		18455
#define ALPHA 		2300
#else
#include "def.h"
#endif /* TEST_KERN */
#include <complex.h>
#include "mkl_types.h"

typedef enum {
	MOD_PSK = 1,
	MOD_QPSK = 2,
	MOD_16QAM = 4,
	MOD_64QAM = 6
} mod_type;

typedef MKL_Complex8 scData_t[MAX_SC];
typedef scData_t rxData_t[RX_ANT];
typedef rxData_t layerData_t[MAX_LAYERS];

typedef MKL_Complex8 weight_t[RX_ANT];
typedef weight_t weightSC_t[MAX_SC];

typedef MKL_Complex8 complexMatrix_t[RX_ANT][MAX_LAYERS];
typedef MKL_Complex8 channelEst_t[RX_ANT][MAX_LAYERS][MAX_RB];
typedef scData_t combWeights_t[MAX_LAYERS][RX_ANT];
typedef MKL_Complex8 combWeightsPerLayer_t[MAX_SC][RX_ANT];
typedef int __fixed;

#endif
