/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/

#ifndef _SOFT_DEMAP_H
#define _SOFT_DEMAP_H

#include "kernel_def.h"
#include "mkl.h"

//void soft_demap(complex* in, int noise, int mod, int n, char* out);
void soft_demap(MKL_Complex8 *in, int noise, int mod, int n, char* out);
//void soft_demap_pthread(complex* in, int scaling_factor, int mod, int n, char* out, int num_threads, int id);
void soft_demap_pthread(MKL_Complex8 *in, float scaling_factor, int mod,\
						int n, char* out, int num_threads, int id);

void soft_demap_pthread_test(int nmbRB);
#endif
