
#include "common_utils.h"

/* Compute t2 - t1 */

inline long long calculate_time_diff_spec(struct timespec t2, struct timespec t1) {
    long long elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000000000LL + t2.tv_nsec - t1.tv_nsec;
	return elapsedTime;
}

/* generate a complex vector of size n */
void gen_vector(MKL_Complex8 *out, MKL_INT size) {
	if(size <= 0) {
		fprintf(stderr,"Size must be non zero\n");
		exit(EXIT_FAILURE);
	}
	int i = 0;
	float re = 0.0, im = 0.0;
	if(out) {
		for(i = 0; i < size; i++) {
			out[i].real = rand() % (0xff);
			out[i].imag = rand() % (0xff);
			//out[i] = re + im * I;
		}
	}
	else {
		fprintf(stderr,"The output complex vector holder not valid\n");
		exit(EXIT_FAILURE);
	}
}

void gen_scalar(MKL_Complex8 *out) {
	float re = 0, im = 0;
	if(out) {
		out->real = rand() % (0xff);
		out->imag = rand() % (0xff);
	}
	else {
		fprintf(stderr,"The output complex vector holder not valid\n");
		exit(EXIT_FAILURE);
	}
}
