/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/

#ifndef _MF_H
#define _MF_H

#include "complex_def.h"
#include "mkl.h"

void mf(const MKL_Complex8 *in, const MKL_Complex8 *rs, const MKL_INT n,\
		MKL_Complex8 *out, float *power);
void mf_test(int nmbRB);

#endif
