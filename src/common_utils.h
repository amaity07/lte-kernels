#ifndef _COMMON_UTIL_H
#define _COMMON_UTIL_H

#include "complex_def.h"
#include "mkl.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

// Please Also move task queue methods to this file
long long calculate_time_diff_spec(struct timespec, struct timespec);
void gen_vector(MKL_Complex8 *out, MKL_INT size);
void gen_scalar(MKL_Complex8 *out);
//void gen_complex_array(complex )
#endif /* _COMMON_UTIL_H */
