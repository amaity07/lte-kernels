/*******************************************************************************
 *                      LTE UPLINK RECEIVER PHY BENCHMARK                      *
 *                                                                             *
 * This file is distributed under the license terms given by LICENSE.TXT       *
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include "soft_demap_9.h"
#include "common_utils.h"

//void soft_demap(complex* in, int scaling_factor, int mod, int n, char* out) {
void soft_demap(MKL_Complex8 *in, int noise, int mod, int n, char* out) {
//	float L;
//	 
//	int i,k;
	
 //   
//  switch (mod) {
 // case MOD_PSK:
 //   {
 //     int ind = 0;
 //     for (i=0; i<n; i++) {
 //   	out[ind++] = in[i].re * scaling_factor;
 //     }
 //   }
 //   break;
 // case MOD_QPSK:
 //   {
 //     int ind = 0;
 //     complex exp_pi4, temp; 
 //     exp_pi4.re = 4096;
 //     exp_pi4.im = -4096;
 //     for (i=0; i<n; i++) {
 //   	temp = cmul(in[i], exp_pi4);
 //   	out[ind++] = (temp.re * scaling_factor)%255;
 //   	out[ind++] = (temp.im * scaling_factor)%255;
 //     }
 //   }
 //   break;
 // case MOD_16QAM:
 //   {
 //     int temp_real, temp_imag;
 //     int ind = 0;
 //     
 //     for (i=0; i<n; i++) {
 //   	for (k=0; k<4; k++) {
 //     		L = k*4096;
 //     		temp_real = (int)(((L - 1) - ((in[i].re * scaling_factor)>>8) + (L - 1)) / 2.0);
 //     		temp_imag = (int)(((L - 1) - ((in[i].im * scaling_factor)>>8) + (L - 1)) / 2.0);
 //     		if (temp_real < 0)
 //     		  temp_real = 0;
 //     		else if (temp_real > (L - 1))
 //     		  temp_real = (L - 1);
 //     		if (temp_imag < 0)
 //     		  temp_imag = 0;
 //     		else if (temp_imag > (L - 1))
 //     		  temp_imag = (L - 1);
 //     		out[ind++] = temp_real%255;
 //     		out[ind++] = temp_imag%255;
 //   	}
 //     }
 //   }
 //   break;
 // case MOD_64QAM:
 //   {
 //     float temp_real, temp_imag;
 //     int ind = 0;
 //     
 //     for (i=0; i<n; i++) {
 //   	for (k=0; k<6; k++) {
 //     		L = k*4096;
 //     		temp_real = (((L - 1) - ((in[i].real * scaling_factor)/256.0) + (L - 1)) / 2.0);
 //     		temp_imag = (((L - 1) - ((in[i].imag * scaling_factor)/256.0) + (L - 1)) / 2.0);
 //     		if (temp_real < 0)
 //     		  temp_real = 0;
 //     		else if (temp_real > (L - 1))
 //     		  temp_real = (L - 1);
 //     		if (temp_imag < 0)
 //     		  temp_imag = 0;
 //     		else if (temp_imag > (L - 1))
 //     		  temp_imag = (L - 1);
 //     		out[ind++] = temp_real;
 //     		out[ind++] = temp_imag;
 //   	}
 //     }
 //   }
 //   break;
 // default:
 //   fprintf(stderr,"Modulation not supported: %i\n", mod);
 // }
}

/* Parallelized symbol demap */
inline void soft_demap_pthread(MKL_Complex8 *in, float scaling_factor, int mod,\
						int n, char* out, int num_threads, int id) {
  	float L;
	int32_t 	 L_fixedpt;
	int16_t		 real_fixedpt, imag_fixedpt;
  	int i,k,ind;

    unsigned int work_per_thread = n/num_threads;

 // //printf("demap-id:%dentered, work per thread\n",id,work_per_thread);
  switch (mod) {
 // case MOD_PSK:
 //   {
 //     //printf("demap-id:%d,executing psk\n",id);
 //     for (i=id*work_per_thread; i<(id+1)*work_per_thread; i++) {
 //     	ind = i;
 //   	out[ind++] = in[i].re * scaling_factor;
 //     }
 //   }
 //   break;
 // case MOD_QPSK:
 //   {
 //     //printf("demap-id:%d,executing qpsk\n",id);
 //     complex exp_pi4, temp; 
 //     exp_pi4.re = 4096;
 //     exp_pi4.im = -4096;
 //     for (i = id*work_per_thread; i < (id+1)*work_per_thread; i++) {
 //     	ind = 2*i;
 //   	temp = cmul(in[i], exp_pi4);
 //   	out[ind++] = (temp.re * scaling_factor)%255;
 //   	out[ind++] = (temp.im * scaling_factor)%255;
 //     }
 //   }
 //   break;
 // case MOD_16QAM:
 //   {
 //    //printf("demap-id:%d,executing 16qam\n",id);
 //    int temp_real, temp_imag;
 //     
 //     ind = 0;
 //     for (i = id*work_per_thread; i < (id+1)*work_per_thread; i++) {
 //   	ind = i*2*4;
 //   	for (k=0; k<4; k++) {
 //     		L = k*4096;
 //     		temp_real = (int)(((L - 1) - ((in[i].re * scaling_factor)>>8) + (L - 1)) / 2.0);
 //     		temp_imag = (int)(((L - 1) - ((in[i].im * scaling_factor)>>8) + (L - 1)) / 2.0);
 //     		if (temp_real < 0)
 //     		  temp_real = 0;
 //     		else if (temp_real > (L - 1))
 //     		  temp_real = (L - 1);
 //     		if (temp_imag < 0)
 //     		  temp_imag = 0;
 //     		else if (temp_imag > (L - 1))
 //     		  temp_imag = (L - 1);
 //     		out[ind++] = temp_real%255;
 //     		out[ind++] = temp_imag%255;
 //   	}
 //     }
 //   }
 //   break;
      case MOD_64QAM:
    {
      //printf("demap-id:%d,executing 64qam\n",id);
      int32_t 	temp_real, temp_imag; 
      uint32_t 	temp_realu, temp_imagu; /* Unsigned Values */
      int32_t 	scaling_factor_fixedpt = (int32_t)scaling_factor;
      ind = 0;
      for (i = id*work_per_thread; i < (id+1)*work_per_thread; i++) {
    	ind = i*2*6;
    	for (k=0; k<6; k++) {
			/* Convert to fixed point */
      		L_fixedpt 		= k << 12; /* multiply by 4096 */
			real_fixedpt	= (int16_t)in[i].real;
			imag_fixedpt	= (int16_t)in[i].imag;
			
			/* Compute intermediate values */
      		temp_real = (((L_fixedpt - 1) - ((real_fixedpt * scaling_factor_fixedpt) >> 8) + (L_fixedpt - 1)) >> 1);
      		temp_imag = (((L_fixedpt - 1) - ((imag_fixedpt * scaling_factor_fixedpt) >> 8) + (L_fixedpt - 1)) >> 1);

			/* Restrict the values */
      		if (temp_real < 0)
      			temp_realu = 0;
      		else if (temp_real > (L_fixedpt - 1))
      			temp_realu = (L_fixedpt - 1);
      		if (temp_imag < 0)
      			temp_imagu = 0;
      		else if (temp_imag > (L_fixedpt - 1))
      			temp_imagu = (L_fixedpt - 1);

			/* Assign value to the output array */
      		out[ind++] = (temp_realu & 0x000000ff);
      		out[ind++] = (temp_imagu & 0x000000ff);
    	}
      }
    }
 
	break;

  default:
    fprintf(stderr,"(DEMAP) Modulation not supported: %i\n", mod);
  }
}

#ifdef TEST_KERN
char softbits[2*2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS];
MKL_Complex8 in[2*(OFDM_IN_SLOT-1)*MAX_SC*MAX_LAYERS];
void soft_demap_pthread_test(int nmbRB) {
	int nmbSc 		= nmbRB * 12;
	int nmbSymbols  = NMB_SLOT*nmbSc*(OFDM_IN_SLOT-1)*4;
	//char softbits[2*2*(OFDM_IN_SLOT-1)*MAX_SC*MOD_64QAM*MAX_LAYERS];
	//MKL_Complex8 in[2*(OFDM_IN_SLOT-1)*MAX_SC*MAX_LAYERS];
	gen_vector(in,2*(OFDM_IN_SLOT-1)*MAX_SC*MAX_LAYERS);

	int i;	
	FILE* fd 	 = fopen("log/demap_ph4.csv","w");
	if(!fd) {
		fprintf(stderr,"Unable to open file\n");
		exit(EXIT_FAILURE);
	}
	struct timespec start, end;
	long long time_diff;
	
	fprintf(fd,"#demap-sample(RB=%d),time(in ns)\n",nmbRB);
	for(i = 0; i < SAMPLES; i++) {
		clock_gettime(CLOCK_MONOTONIC,&start);
		soft_demap_pthread(in, 3, MOD_64QAM, nmbSymbols, softbits, 24, 0);
		clock_gettime(CLOCK_MONOTONIC,&end);
		time_diff = calculate_time_diff_spec(end,start);
		fprintf(fd,"sample-%d,%lld\n",i,time_diff);
	}
	fclose(fd);	
	//printf("Time Consumed (in ns) = %lld\n",time_diff);
}
#endif
