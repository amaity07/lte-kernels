ROOT	   = ${PWD}
KERNELS    = ${PWD}/src
LIBS	   = -lpthread -lm
INCLUDES   = -I ${KERNELS} -I ${ROOT}/phy
DFLAGS	   = -DCONTROLLED2_MAXUSERS=${user} -DCONTROLLED2_PRBS=${prbs} -DTEST_KERN
CC		   = ${cc}

# Compiler Specific optimization options
ifeq (${cc},icc)
	#CFLAGS     = -O0 -mkl -g
	CFLAGS     = -xHost -mkl
	#CFLAGS     = -xMIC-AVX512
else
	CFLAGS	   = -O3
endif

.PHONY: all
all: kernel_analysis

# Isolated Kernel Analyses
KERNEL_SRCS = \
	${KERNELS}/ant_comb_7.c						\
	${KERNELS}/chest_5.c						\
	${KERNELS}/complex_def.c					\
	${KERNELS}/fft_8.c							\
	${KERNELS}/interleave_11.c					\
	${KERNELS}/soft_demap_9.c					\
	${KERNELS}/mf_4.c							\
	${KERNELS}/weight_calc_6.c					\
	${KERNELS}/mmse_by_cholsolve_4xX_complex.c	\
	${KERNELS}/crc_13.c							\
	${KERNELS}/turbo_dec_12.c					\
	${KERNELS}/common_utils.c					\
	${ROOT}/phy/util.c							\
	${ROOT}/phy/phy_ul_barrier.c				\
	${ROOT}/phy/phy_ul_MS.c						\
	${ROOT}/phy/phy_ul_MS_mue.c					\
	${ROOT}/phy/phy_ul_MS_mue_serial.c			\
	${ROOT}/kernel_analysis.c									

KERNEL_OBJS=$(KERNEL_SRCS:.c=.o)

.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) $(DFLAGS) -c $< -o $@

kernel_analysis: $(KERNEL_OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) $(DFLAGS) $(KERNEL_OBJS) -o kernel_analysis $(LIBS)

compile:
	$(CC) $(CFLAGS) $(INCLUDES) $(DFLAGS) $(KERNEL_SRCS) -o kernel_analysis $(LIBS)

clean:
	rm -rf $(KERNEL_OBJS)
	rm -rf kernel_analysis
