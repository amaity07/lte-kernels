#include <stdio.h>
#include <stdlib.h>
#include "mkl.h"

//typedef double lte_data_t;
int main() {
	double *A, *B, *C;
	int m, n, k, i, j;
	double alpha = 1.0, beta = 0.0;
	
	m = n = k = 1200;
	A = (double *)mkl_malloc( m*k*sizeof( double ), 64 );
	B = (double *)mkl_malloc( k*n*sizeof( double ), 64 );
	C = (double *)mkl_malloc( m*n*sizeof( double ), 64 );

    if (A == NULL || B == NULL || C == NULL) {
      printf( "\n ERROR: Can't allocate memory for matrices. Aborting... \n\n");
      mkl_free(A);
      mkl_free(B);
      mkl_free(C);
      exit(EXIT_FAILURE);
    }

    printf (" Intializing matrix data \n\n");
    for (i = 0; i < (m*k); i++) {
        A[i] = (double)(rand() % 89);
    }
    for (i = 0; i < (k*n); i++) {
        B[i] = (double)(rand() % 101);
    }
    for (i = 0; i < (m*n); i++) {
        C[i] = 0.0;
    }

	mkl_set_num_threads(1);
	printf("Starting CBLAS with #%d, threads\n",mkl_get_max_threads());
	cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,\
	m, n, k, alpha, A, k, B, n, beta, C, n);

	printf("Computation successful\n");

	mkl_free(A);
	mkl_free(B);
	mkl_free(C);
}
