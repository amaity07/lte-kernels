#!/bin/bash

source /opt/intel/compilers_and_libraries_2017.2.174/linux/bin/iccvars.sh intel64
rm -rf log*
export MKL_NUM_THREADS=1
make cc=icc
mkdir -p log
mkdir -p log1
mkdir -p mue_serial_dataset_pipelinedsf
#for user in $( seq 1 10 ); do
#	for prbs in $(seq $user $user 100); do
#		#prbs=10
#		#user=10
#		prbs1=$((${prbs}/${user}))
#		echo prbs: $prbs, users: $user, prbs1: ${prbs1}
#		(taskset -c 0-31 ./kernel_analysis ${prbs1} ${user} log 0 0) & (taskset -c 32-63 ./kernel_analysis ${prbs1} ${user} log1 31 1) & wait
#		mv log/sf_f.csv mue_serial_dataset_pipelinedsf/sample-user${user}-prbs${prbs}-layer4.csv
#	done
#done
prbs=50
user=2
prbs1=$((${prbs}/${user}))
taskset -c 0-31 ./kernel_analysis ${prbs1} ${user} log 0 0
